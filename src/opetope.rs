pub trait Cell {
    fn get_ins(&self) -> &[Label];
    fn get_out(&self) -> &Label;

    fn get_name(&self) -> &Label;

    fn enclose(self) -> Group;// TODO: Maybe use an associated type?
}

/// An opetope component that can contain other components.
/// It is bound to a node in the next diagram.
///
pub struct Group {
    ins: Vec<Label>,
    out: Label,
    name: Label,

    interior: Vec<Box<dyn Cell>>,
}

/// An opetope component that cannot contain anything.
/// It is bound to an input in the next diagram.
///
pub struct End {
    ins: Vec<Label>,
    out: Label,

    name: Label,
}

/// A selector of a subtree of the current group interior.
///
pub struct Subgroup<'s> {
    ins: Vec<&'s Label>,
    out: &'s Label,
}

/// A label for the opetope components.
///
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Label {
    name: String,

    is_special: bool,
    subsection: Option<String>,
}


// Constructors
//
impl Group {
    /// Creates a node without sources, named "__init".
    ///
    pub fn initial() -> Self {
        Group {
            ins: Vec::new(),
            out: Label::from("__init").special(),
            name: Label::from("__init").special(),

            interior: Vec::new(),
        }
    }
}


impl Label {
    // Toggles the `is_special` flag.
    //
    fn special(self) -> Self {
        let Label { name, subsection, is_special } = self;

        Label {
            name,
            subsection,
            is_special: !is_special,
        }
    }
}


// [AREA] Trait Impls
//
macro_rules! impl_cell {
    ( $this:ident ) => {
        impl Cell for $this {
            fn get_ins(&self) -> &[Label] {
                &self.ins
            }
            fn get_out(&self) -> &Label {
                &self.out
            }
            fn get_name(&self) -> &Label {
                &self.name
            }

            fn enclose(self) -> Group {
                Group {
                    ins: self.ins.clone(),
                    out: self.out.clone(),
                    name: "".into(),

                    interior: vec![Box::new(self)],
                }
            }
        }
    };
}

impl_cell! { Group }
impl_cell! { End }
//
// [END] Trait Impls

// [AREA] Conversions
//
impl<S: ToString> From<S> for Label {
    fn from(name: S) -> Self {
        Label {
            name: name.to_string(),
            subsection: None,
            is_special: false,
        }
    }
}
//
// [END] Conversions
